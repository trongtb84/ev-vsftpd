#FROM quay.io/centos/centos:7.5.1804
FROM quay.io/centos/centos:8.4.2105
MAINTAINER trongtb5@fpt.com.vn
LABEL Description="proftpd Docker image based on Centos 7"
RUN dnf install -y \
    epel-release \
	ftp 
RUN dnf install -y proftpd
RUN useradd -u 4000 -p '$1$MYmn2Yqc$CnzM8.vcO/Tgv.grSFpJ/1' ftpuser
RUN chown 4000:4000 /home/ftpuser
#COPY run-proftpd.sh /usr/sbin/
#RUN chmod +x /usr/sbin/run-proftpd.sh
EXPOSE 20 21 49152 49153 49154
#CMD ["/usr/sbin/run-proftpd.sh"]
ENTRYPOINT ["usr/sbin/proftpd"]
CMD ["--nodaemon","-c","/etc/proftpd.conf"]
